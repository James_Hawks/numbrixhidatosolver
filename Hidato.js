/* ========================================================
** Author :   James D. Hawks
** Date :     2-8 April 2020. 
** Purpose :  Use JavaScript to solve Hidato puzzle published in Daily News newspaper.
** Method :   Brute force search with constraints.
** Based On : 10/2010, Continuous improvements through 11/2011, for KenKen. 
**            17 January 2007, Java version of Sudoku Solver
**
**   Although similar, this code differs from both the Sudoku Solver and KenKen Solver codes :  
**   - Hidato uses a 2D object structure while the others rely on a linear index.
**   - Different variables used to specify values on left side of board.
**     + Hidato: window.document.the_form.activate_disable.value
**     + KenKen: window.document.the_form.currentGroup.value
**     + Sudoku: window.document.the_form.which_value.value 
**     
**
**  user_interaction was essentially the main routine as it controled the user interface.
**   
**  If memory servers me correctly, the reason that images were used inside of the buttons,
**  instead of text, was because the user actually had to select the text itself,
**  not just anywhere inside the button.  Which meant most button strikes became misses.
**  More than a decade has brought enough improvements that I have switched from images to text.
** 
**   
*/
function canImoveThere( current_value, target_position, search_up_or_down )
 {
  /* ================================================================
  ** Author:  James D. Hawks
  ** Date:    5,16 April 2020 
  ** Purpose: Return value indicating whether or not move to target position is acceptable.
  **          So apply rules governing moves to a single, potential step.
  */
  var valid = 1 ;
  // Can't move to disabled square.
  if ( target_position.a_or_d == 0 ) valid = 0 ;
  /*
  ** Must remove zero from consideration because that is the value assigned 
  ** to denote an empty position where a value can be placed.
  */
  if ( target_position.a_or_d == 1 ) 
   {
    if ( target_position.fixed  == 1 )
	 {
      /*
      ** When the value is fixed, specified by user, 
      ** the value must be one greater than yours, 
      ** unless searching backwards, then one less.
      */
      if ( ( current_value + search_up_or_down ) != target_position.value ) valid = 0 ;
     }
    else
	 {
      // Can't move to occupied position.
      if ( target_position.value != 0 ) valid = 0 ;
     }
   }
  //
  return valid ;
 }
function clear( )
 {
  /* ================================================================
  ** Author:  James D. Hawks
  ** Date:    5 September 2005 in Java
  ** Translation to JavaScript: 20 January 2007
  ** Purpose: Remove entries and prepare for another execution.
  */
  var i, j, temp, numRow, numCol, countButton ;
  //alert("subroutine clear") ;
  numRow     = parseInt(window.document.the_form.numberRows.value)    ;
  numCol     = parseInt(window.document.the_form.numberColumns.value) ;
  for (j=0; j<numRow; ++j)
   {
    for (i=0; i<numCol; ++i)
     {
      storeValue( j, i, 0 ) ;
     }
   }
  // Initialize the count button.
  countButton = window.document.getElementById( "control_13" ) ;
  place_label_on_the_button( countButton, "Count" ) ;
  /*
  ** The name says it all.
  ** Must be after the above zeroing, or the button assignments fail.
  */
  delete_the_board() ;
  /*
  */
  window.document.the_form.verify_error.value      = -1  ;
  window.document.the_form.step_number.value       = 1   ;
  window.document.the_form.numberColumns.value     = 0   ;
  window.document.the_form.numberRows.value        = 0   ;
  window.document.the_form.activate_disable.value  = 1   ;
  window.document.the_form.current_row.value       = 0   ;
  window.document.the_form.current_column.value    = 0   ;
  window.document.the_form.numbrix_or_hidato.value = 1   ;
  window.document.the_form.communicate.value       = 
    "Use keypad below to enter the number of vertical columns on the board.  Current value is 0.  Hit 'Next Step' when done.  The number of columns and rows taken together should describe a rectangle enclosing each position. " ;
  clear_solutions( ) ;
 }
function clear_solutions( )
 {
  /* ================================================================
  ** Author:  James D. Hawks
  ** Date:    16 May 2020 
  ** Purpose: Separated from clear, above, because now called in two places.
  */
  window.document.the_form.number_of_solutions.value  = 0 ;
  window.document.the_form.current_solution.value     = 0 ;
  window.document.the_form.solution_01.value   = "" ;
  window.document.the_form.solution_02.value   = "" ;
  window.document.the_form.solution_03.value   = "" ;
  window.document.the_form.solution_04.value   = "" ;
  window.document.the_form.solution_05.value   = "" ;
  window.document.the_form.solution_06.value   = "" ;
  window.document.the_form.solution_07.value   = "" ;
  window.document.the_form.solution_08.value   = "" ;
  window.document.the_form.solution_09.value   = "" ;
  window.document.the_form.solution_10.value   = "" ;
  window.document.the_form.solution_11.value   = "" ;
  window.document.the_form.solution_12.value   = "" ;
  window.document.the_form.solution_13.value   = "" ;
  window.document.the_form.solution_14.value   = "" ;
  window.document.the_form.solution_15.value   = "" ;
  window.document.the_form.solution_16.value   = "" ;
  window.document.the_form.solution_17.value   = "" ;
  window.document.the_form.solution_18.value   = "" ;
  window.document.the_form.solution_19.value   = "" ;
  window.document.the_form.solution_20.value   = "" ;
 }
function compare_board ( row, column, numRow, numCol, position_list, minimum, maximum )
 {
  /* ================================================================
  ** Author:  James D. Hawks
  ** Date:    7 April 2020, based on 7 October 2011 fro Sudoku
  ** Purpose: Allow the comparison of current board arrangement within   
  **          search to a given value, possibly a known solution.
  ** Method:  Values assigned previously will be equal to the given value
  **          and the current value, at the current index, 
  **          will become greater than the given value when the search
  **          crosses from one side of the given to the other.
  **          Allow a separate line to be executed so debugger 
  **          can be stopped there.
  **          Enter known solution as array "known_board."
  **
  ** Update:  In Hidato grid is no longer rectangular,         
  **          but we use zeros for disabled postions, so enter them here.
  ** Problem: Hidato does NOT make left to right, top to bottom, search starting in upper left.
  **          So the search may never cross from below to above know solution.
  **          Therefore, we are going to have to translate both to ordered lists
  **          and evaluate row, column of a number to see when moved across the
  **          correct possibility.  At his time possibilities were assigned 
  **          left to right, top to bottom, in the square surrounding the origin.
  **          This will generate a crossing over when move from one possibility to
  **          another within that surrounding square.
  **
  ** 
  ** #################################################################################
  ** ##### KNOWN_BOARD MUST HAVE SAME NUMBER OF ROWS/COLUMNS                    ######
  ** ##### COMMENT OUT THIS SLOW ROUTINE WHEN NOT IN USE, AND ON SMALLER CASES  ######
  ** #################################################################################
  ** Comment out the call, not here.
  */
  var i, j, run=1, index=1, flag=0, LI_board, LI_solution, temp ;
  var rowMin, colMin, minValue, rowMax, colMax, maxValue ;
  var known_Board = {}, known_Board_array = {}, OL_solution = {}, OL_board = {} ;
  known_Board_array[0] = new Array( 10,12,14,15,34,35,36 ) ;
  known_Board_array[1] = new Array(  9,11,13,16,32,33,37 ) ;
  known_Board_array[2] = new Array(  7, 8,20,17,31,40,38 ) ;
  known_Board_array[3] = new Array(  6,19,18,21,41,30,39 ) ;
  known_Board_array[4] = new Array(  5, 4, 2,42,22,24,29 ) ;
  known_Board_array[5] = new Array( 49, 3, 1,43,23,28,25 ) ;
  known_Board_array[6] = new Array( 48,47,46,45,44,26,27 ) ;  
  // Here is a small case I use when just checking that everything still works :
  // known_Board_array[0] = new Array(  2, 4 ) ;
  // known_Board_array[1] = new Array(  1, 3 ) ;
  // Copy the known board from an array of base type to an array of Position Objects.
  for ( j=0; j<numRow; ++j )
   {
    known_Board[j] = {} ;
    for ( i=0; i<numCol; ++i )
	 {
      known_Board[j][i] = new position ( j, i, 0, 0, known_Board_array[j][i] ) ;
     }
   }
  //
  rowMin   = minimum[0] ;
  colMin   = minimum[1] ;
  minValue = minimum[2] ;
  rowMax   = maximum[0] ;
  colMax   = maximum[1] ;
  maxValue = maximum[2] ;
  /*
  ** Generate ordered lists for solution and current board.
  ** Notice that current board may not have every value, but will have the min and max.
  */
  generateOrderedList( OL_solution, numRow, numCol, known_Board,   minValue, maxValue ) ;
  generateOrderedList( OL_board,    numRow, numCol, position_list, minValue, maxValue ) ;
  /*
  ** Loop over entries in ordered list of board, 
  ** but stop when encounter a missing value, because search has not gone that far yet.
  ** Both start at same place, the fixed minValue, so no need to compare first entry.
  */
  while ( run == 1 && OL_board[ index ] != null  )
   {
    if ( parseInt( OL_board[ index ].value ) != ( 1 + parseInt( OL_board[ index - 1 ].value ) ) )
     {
      run = 0 ;
     }
    else
	 {
      // LI = linear index
      LI_board    = ( parseInt(    OL_board[ index ].row ) * numCol ) + parseInt(    OL_board[ index ].column ) ;
      LI_solution = ( parseInt( OL_solution[ index ].row ) * numCol ) + parseInt( OL_solution[ index ].column ) ;
      /*
      ** End comparison when the board position is less than known,
      ** Because later digits could end up greater, but overall value less due larger digits being less.
      */
      if ( LI_board < LI_solution ) run = 0 ;
      if ( LI_solution < LI_board ) 
	   {
        /*
        ** This statement executed first time when cross over known value. 
        */
        flag = 1 ;
       }
      if ( (row==43) && (column==34) && (index==3) )
	   {
        /*  Stop on this statement after a cross over point located.  */
        temp = 1 ;
       }
     }
    index++ ;
   }
 }
function count_unknown_values ( numRow, numCol ) 
 {
  /* ================================================================
  ** Author:  James D. Hawks
  ** Date:    12 April 2020 
  ** Purpose: Count the number of positions where the value must be determined.
  **          Have to use the button definitions, because have not loaded
  **          that information into the arrays yet.
  ** 
  */
  var i, j, numberUnknown=0, buttonIdString, theButtonElement ;
  for ( j=0; j<numRow; ++j ) 
   {
    for ( i=0; i<numRow; ++i )
     {
      buttonIdString = "field_" + j.toString(10) + "_" + i.toString(10) ;
      theButtonElement  = window.document.getElementById( buttonIdString ) ;
      if ( 0 == parseInt( theButtonElement.getAttribute( "fixed_value"  ) ) &&
           1 == parseInt( theButtonElement.getAttribute( "a_or_d" ) )  )
       {
		numberUnknown++ ;
       }
     }
   }
  return numberUnknown ;
 }
function count_visits ( row, column, visit_counting, theValue ) 
 {
  /* ================================================================
  ** Author:  James D. Hawks
  ** Date:    8 April 2020 
  ** Purpose: Count the number of visits to a position,
  **          and track the value when visit.
  **          
  */
  var flag_1=0, flag_2=0 ;
  visit_counting[ row ][ column ][ 0 ]++ ;
  if ( visit_counting[ row ][ column ][ theValue ] == null ) 
   {
    visit_counting[ row ][ column ][ theValue ] = 1 ;
   }
  else
   {
    visit_counting[ row ][ column ][ theValue ]++ ;
   }
  if ( 200 < visit_counting[ row ][ column ][ 0 ] )
   {
    flag_1 = 1 ;
   }
  // The number 8 is the number of possibilities around an origin.
  if ( 20 < visit_counting[ row ][ column ][ theValue ] )
   {
    flag_2 = 1 ;  
   }
 }
function create_the_board()
 {
  /* ================================================================
  ** Author:  James D. Hawks
  ** Date:    2,4,8 January 2011 
  ** Purpose: Generate a table of buttons, to left of current presentation, 
  **          with the specified number of rows and columns.
  **          Modify existing outer tables for new sizes.
  **          
  **          4/2/2020 - table_center_divider added.
  */
  var unit_height=40, unit_width=40, unit_cellspacing=2, tempArray, temp ;
  var table_main, table_left_outer, table_left_inner, table_center_divider ;
  var valid_or_invalid, numRow, numCol, table_left_inner_height, table_left_inner_width ;
  var table_left_outer_height, table_left_outer_width, height_increment=0, next_height_increment ;
  var table_center_divider_height, table_center_divider_width, numberUnknown, countButton ; 
  var tbodyNode, rowNode, cellNode, textNode, buttonNode, imageNode, textStringToPlace ;
  table_main                = window.document.getElementById("table_main"               ) ;
  table_left_outer          = window.document.getElementById("table_left_outer"         ) ;
  table_left_inner          = window.document.getElementById("table_left_inner"         ) ;
  table_center_divider      = window.document.getElementById("table_center_divider"     ) ;  
  /*
  ** Verify that correct elements returned.
  */
  valid_or_invalid = ( table_main                                           && 
                       table_main.nodeType == 1                             &&  
                       table_main.tagName.toUpperCase() == "TABLE"           ) ;
  valid_or_invalid = ( valid_or_invalid                                     &&
                       table_left_outer                                     && 
                       table_left_outer.nodeType == 1                       &&  
                       table_left_outer.tagName.toUpperCase() == "TABLE"     ) ;
  valid_or_invalid = ( valid_or_invalid                                     &&
                       table_left_inner                                     && 
                       table_left_inner.nodeType == 1                       &&  
                       table_left_inner.tagName.toUpperCase() == "TABLE"     ) ;
  valid_or_invalid = ( valid_or_invalid                                     &&
                       table_center_divider                                 && 
                       table_center_divider.nodeType == 1                   &&  
                       table_center_divider.tagName.toUpperCase() == "TABLE" ) ;					   
  if ( valid_or_invalid )
   {
    /*
    ** Compute the size of the inner table, and the wrapping layers.
    */    
    numRow   = parseInt(window.document.the_form.numberRows.value) ;
    numCol   = parseInt(window.document.the_form.numberColumns.value) ;
    table_left_inner_height = ( numRow * unit_height ) +
                              ( ( numRow + 1 ) * unit_cellspacing ) ;
    table_left_inner_width  = ( numCol * unit_width  ) +
                              ( ( numCol + 1 ) * unit_cellspacing ) ;
    table_left_outer_height = table_left_inner_height + ( 0 * unit_cellspacing ) ;
    table_left_outer_width  = table_left_inner_width  + ( 0 * unit_cellspacing ) ;
    height_increment = table_left_outer_height - table_main.getAttribute( "height" ) -
              ( 2 * table_main.getAttribute( "cellspacing" ) ) ;
    if ( height_increment < 0 ) height_increment = 0 ;
    next_height_increment = 36 ;
	table_center_divider_height = table_left_outer_height ;
	table_center_divider_width  = 0 ;
	if ( numRow != 0 && numCol !=0 ) table_center_divider_width = unit_width ;
    /*
    ** Make changes to outside tables first, then inside tables,
    ** so that never asking to place larger table in smaller area.
    **
    ** Notice that the call to getElementsByTagName got all entries, not just the top level.
    */
    tempArray = table_main.getElementsByTagName("td") ;
    table_main.setAttribute( "width",   parseInt( table_main.getAttribute("width") ) +
                                        parseInt( table_left_outer_width           ) +
                                        parseInt( table_center_divider_width       ) ) ;
    table_main.setAttribute( "height",  parseInt( table_main.getAttribute( "height") ) +
                                        parseInt( height_increment       ) ) ;
    tempArray = table_main.getElementsByTagName("tr") ;
    tempArray[0].setAttribute( "width", parseInt( tempArray[0].getAttribute( "width" ) ) + 
                                        parseInt( table_left_outer_width               ) +
                                        parseInt( table_center_divider_width           ) ) ;
    tempArray = tempArray[0].getElementsByTagName("td") ;
    //for (i=0; i<tempArray[0].childNodes.length; ++i) 
    for (i=0; i<tempArray.length; ++i) 
     { 
      if ( ( tempArray[i].getAttribute ( "id" ) == "hold_margin_left"          ) ||
           ( tempArray[i].getAttribute ( "id" ) == "hold_table_left_outer"     ) ||
           ( tempArray[i].getAttribute ( "id" ) == "hold_table_right_outer"    ) ||
           ( tempArray[i].getAttribute ( "id" ) == "hold_margin_right"         ) ||
           ( tempArray[i].getAttribute ( "id" ) == "hold_table_center_divider" )  )
       {
        tempArray[i].setAttribute( "height", 
                         ( parseInt( tempArray[i].getAttribute( "height" ) ) + 
                           parseInt( height_increment ) ) ) ;
       }
     }
    table_left_outer.setAttribute( "height", table_left_outer_height ) ;
    table_left_outer.setAttribute( "width",  table_left_outer_width  ) ;
    table_left_inner.setAttribute( "height", table_left_inner_height ) ;
    table_left_inner.setAttribute( "width",  table_left_inner_width  ) ;
	table_center_divider.setAttribute( "height", table_center_divider_height ) ;
    table_center_divider.setAttribute( "width",  table_center_divider_width  ) ;
    /*
    ** Fill the left inner table. 
    ** Table body node stuff from book, Murach, page 593.
    */
    if ( table_left_inner.tBodies.length == 0 )
     {
      tbodyNode = document.createElement("tbody");
      table_left_inner.appendChild(tbodyNode); 
     }
    else
     { 
      tbodyNode = table_left_inner.tBodies[0] ;
     }
    for ( j=0; j<numRow; ++j )
     {
      rowNode = tbodyNode.insertRow(-1) ;
      rowNode.setAttribute("width", table_left_inner_width) ;
      for ( i=0; i<numCol; ++i )
       {
        /*
        ** Use of the DOM_tree_walk on FireFox shows that
        ** the button and image are each a type 1 node.
        ** So try to create them that way.
        */
        cellNode = document.createElement("td") ;
        cellNode.setAttribute("height",   unit_height) ;
        cellNode.setAttribute("width",    unit_width ) ;
        cellNode.setAttribute("align",    "center"   ) ;
        textStringToPlace = "field_" + j.toString(10) + "_" + i.toString(10) ;
        buttonNode = document.createElement("button") ;
        // BOTH of these work :
        buttonNode.textContent  = " "  ;
        //buttonNode.innerHTML  = " "  ;		
        //
        buttonNode.setAttribute("type",   "button"         ) ;
        buttonNode.setAttribute("id",     textStringToPlace) ;
        buttonNode.setAttribute("name",   textStringToPlace) ;
        buttonNode.setAttribute("class",  "button_left"    ) ;
        /*
        ** Add a few attributes to the button for object oriented storage.
        */
        buttonNode.setAttribute("a_or_d",      "1" ) ;
        buttonNode.setAttribute("fixed",       "0" ) ;
        buttonNode.setAttribute("fixed_value", "0" ) ;
        // following line, string taken correctly, but not execute, with or w/o the slashes.
        //textStringToPlace = "setValue(" + j.toString(10) + "," + i.toString(10) + ")\; return false\;" ;
        //alert("textStringToPlace is " + textStringToPlace) ;
        //
        //textNode = document.createTextNode("X") ;
        //buttonNode.setAttribute("onclick", textStringToPlace) ;
        //imageNode = document.createElement("img") ;
        //imageNode.setAttribute("id", textStringToPlace) ;
        //textStringToPlace = "Numbers/none.jpg" ;
        //imageNode.setAttribute("src", textStringToPlace) ;
        //buttonNode.appendChild(imageNode) ;
        //buttonNode.appendChild(textNode) ;
        cellNode.appendChild(buttonNode) ;
        rowNode.appendChild(cellNode) ;
        /*
        ** Since the onclick attribute did not take,
        ** Attach the event handler through JavaScript.
        ** Based on book, Murack, page 493.
        ** But how do we provide arguments to the function ??
        ** Looks like we have to extract them from the button name, inside the function.
        */
        if ( buttonNode.addEventListener )
         {
          // DOM standard.
          buttonNode.addEventListener( "click", setAD, false ) ;
         }
        else if ( buttonNode.attachEvent )
         {
          // MS Internet Explorer.
          buttonNode.attachEvent( "onclick", setAD ) ;
         }
       }
     }
    /*
    ** Update the number of values to be determined, shown in button 13.
    */
    numberUnknown   = count_unknown_values ( numRow, numCol ) ;
    countButton = window.document.getElementById( "control_13" ) ;
    place_label_on_the_button( countButton, numberUnknown.toString(10) ) ;
    /*
    ** Force MicroSoft Internet Explorer to REDRAW entire screen.
    */ 
    window.document.documentElement.style.display = "none" ;
    window.document.documentElement.style.display = "" ;
   }
  else
   {
    alert("ERROR: One of the tables not valid in create_the_board") ;
   }
 }
function delete_the_board()
 {
  /* ================================================================
  ** Author:  James D. Hawks
  ** Date:    8 January 2011 
  ** Purpose: Remove the table of buttons, the left half of the board, 
  **          Reset existing outer tables for smaller sizes.
  **                 
  **          4/2/2020 - table_center_divider added.
  */
  var unit_height=40, unit_width=40, unit_cellspacing=2, tempArray_1, tempArray_2, temp ;
  var table_main, table_left_outer, table_left_inner, table_right_outer, table_center_divider ;
  var valid_or_invalid, numRow, numCol, table_left_inner_height, table_left_inner_width ;
  var table_left_outer_height, table_left_outer_width, height_increment=0, next_height_increment ;
  var table_center_divider_height, table_center_divider_width ;
  var tbodyNode, rowNode, cellNode, textNode, buttonNode, imageNode, textStringToPlace ;
  /*
  ** Compute the current table sizes because MS Internet Explorer can NOT querie the current attributes, in some cases.
  **  
  ** This duplicates computations in create_the_board.
  ** numRow and numCol are values before the delete.
  */
  numRow   = parseInt(window.document.the_form.numberRows.value) ;
  numCol   = parseInt(window.document.the_form.numberColumns.value) ;
  table_left_inner_height = ( numRow * unit_height ) +
                            ( ( numRow + 1 ) * unit_cellspacing ) ;
  table_left_inner_width  = ( numCol * unit_width  ) +
                            ( ( numCol + 1 ) * unit_cellspacing ) ;
  table_left_outer_height = table_left_inner_height + ( 0 * unit_cellspacing ) ;
  table_left_outer_width  = table_left_inner_width  + ( 0 * unit_cellspacing ) ;
  /*
  **
  */
  table_main                = window.document.getElementById("table_main"               ) ;
  table_left_outer          = window.document.getElementById("table_left_outer"         ) ;
  table_left_inner          = window.document.getElementById("table_left_inner"         ) ;
  table_right_outer         = window.document.getElementById("table_right_outer"        ) ;
  table_center_divider      = window.document.getElementById("table_center_divider"     ) ;
  /*
  ** Verify that correct elements returned.
  */
  valid_or_invalid = ( table_main                                           && 
                       table_main.nodeType == 1                             &&  
                       table_main.tagName.toUpperCase() == "TABLE"           ) ;
  valid_or_invalid = ( valid_or_invalid                                     &&
                       table_left_outer                                     && 
                       table_left_outer.nodeType == 1                       &&  
                       table_left_outer.tagName.toUpperCase() == "TABLE"     ) ;
  valid_or_invalid = ( valid_or_invalid                                     &&
                       table_left_inner                                     && 
                       table_left_inner.nodeType == 1                       &&  
                       table_left_inner.tagName.toUpperCase() == "TABLE"     ) ;
  valid_or_invalid = ( valid_or_invalid                                     &&
                       table_right_outer                                    && 
                       table_right_outer.nodeType == 1                      &&  
                       table_right_outer.tagName.toUpperCase() == "TABLE"    ) ;
  valid_or_invalid = ( valid_or_invalid                                     &&
                       table_center_divider                                 && 
                       table_center_divider.nodeType == 1                   &&  
                       table_center_divider.tagName.toUpperCase() == "TABLE" ) ;					   
  if ( valid_or_invalid )
   {
    /*
    ** Find the existing size of the inner table, and the wrapping layers.
    ** Notice two different ways to get and set an attribute.
    **
    ** Do not use these because MS I.E. does not return dynamically set attributes.
    */    
    //table_left_inner_height = table_left_inner.getAttribute("height") ;
    ////table_left_inner_width  = getTheAttribute( table_left_inner, "width"  ) ;
    //table_left_inner_width  = table_left_inner.getAttribute("width" ) ;
    //table_left_outer_height = table_left_outer.getAttribute("height") ;
    //table_left_outer_width  = table_left_outer.getAttribute("width" ) ;
    height_increment = table_right_outer.getAttribute("height") - table_left_outer_height ;
    if ( height_increment > 0 ) height_increment = 0 ;
    /*
    ** Make changes to inside tables first, then outside tables,
    ** so that never asking to place larger table in smaller area.
    **
    ** Remove the left inner table's contents. 
    ** Table body node stuff from book, Murach, page 595.
    */
    while ( table_left_inner.tBodies[0].rows.length > 0 )
     {
      table_left_inner.tBodies[0].deleteRow(-1) ;
     }
    //alert("Before call to setTheAttribute on table_left_inner - height") ;
    //setTheAttribute( table_left_inner, "height", 1 ) ;
    table_left_inner.setAttribute("height", 0 ) ;
    //alert("Before call to setTheAttribute on table_left_inner - width") ;
    //setTheAttribute( table_left_inner, "width",  1 ) ;
    table_left_inner.setAttribute("width",  0) ;
    table_left_outer.setAttribute("height", 0) ;
    table_left_outer.setAttribute("width",  0) ;
	/*
	** Without two tables to separate, collapse the divider table to zero width.
	*/
	table_center_divider.setAttribute( "width",  0  ) ;
    /*
    ** Now work table main, inside to out.
    ** Notice that the call to getElementsByTagName got all entries, not just the top level.
    */
    tempArray_1 = table_main.getElementsByTagName("tr") ;
    tempArray_2 = tempArray_1[0].getElementsByTagName("td") ;
    //for (i=0; i<tempArray_2[0].childNodes.length; ++i) 
    for (i=0; i<tempArray_2.length; ++i) 
     { 
      if ( ( tempArray_2[i].getAttribute ( "id" ) == "hold_margin_left"          ) ||
           ( tempArray_2[i].getAttribute ( "id" ) == "hold_table_left_outer"     ) ||
           ( tempArray_2[i].getAttribute ( "id" ) == "hold_table_right_outer"    ) ||
           ( tempArray_2[i].getAttribute ( "id" ) == "hold_margin_right"         ) ||
           ( tempArray_2[i].getAttribute ( "id" ) == "hold_table_center_divider" )  )
       {
		// BOTH of these are correct, but this whole section taken together does not redraw the board shorter.
		//
        //alert("Before call to setTheAttribute on tempArray_2[i] - height") ;
        //tempArray_2[i].setAttribute( "height", 
        //                 ( parseInt( tempArray_2[i].getAttribute( "height" ) ) + 
        //                   parseInt( height_increment ) ) ) ;
		tempArray_2[i].setAttribute( "height", 403 ) ;
       }
     }
    //alert("Before call to setTheAttribute on tempArray_1[0] - width") ;
    for (i=0; i<tempArray_1.length; ++i) 
     { 
      if ( tempArray_1[i].getAttribute ( "id" ) == "table_mains_one_row" )
       {
        tempArray_1[i].setAttribute( "width", parseInt( tempArray_1[i].getAttribute( "width" ) ) - 
                                              parseInt( table_left_outer_width                 ) - 
                                              parseInt( table_center_divider_width             ) ) ;
       }
     }
    //alert("Before call to setTheAttribute on table_main - width") ;
    table_main.setAttribute( "width",  parseInt( table_main.getAttribute( "width") ) -
                                       parseInt( table_left_outer_width            ) - 
                                       parseInt( table_center_divider_width        ) ) ;
    //alert("Before call to setTheAttribute on table_main - heigth") ;
    //table_main.setAttribute( "height", parseInt( table_main.getAttribute( "height") ) +
    //                                   parseInt( height_increment                   ) ) ;
    table_main.setAttribute( "height", 486 ) ;									   
    /*
    ** Force MicroSoft Internet Explorer to REDRAW entire screen.
    */ 
    window.document.documentElement.style.display = "none" ;
    window.document.documentElement.style.display = "" ;
   }
  else
   {
    alert("ERROR: One of the tables not valid in delete_the_board") ;
   }
 }
function generateOrderedList( ordered_list, numRow, numCol, position_list, minValue, maxValue )
 {
  /* ================================================================
  ** Author:  James D. Hawks
  ** Date:    7 April 2020, 
  **          But just made a routine of existing code few days old.
  ** Purpose: Return an ordered list from min and max arguments.
  **          BUT YOU CAN'T RETURN IT OR PASS BY VALUE IS VIOLATED !!
  **          So you MUST allocate the object in calling routine and fill it here.
  **          Maybe return array, but change top level allocation to
  **          a simple declaration instead of an X = {} ;
  */
  var i, j, k, number_in_list = -1  ;
  //
  for ( k=minValue; k<=maxValue; ++k )
   {
    for ( j=0; j < numRow; ++j )
     {
      for ( i=0; i < numCol; ++i )
       {
        if ( parseInt( position_list[j][i].value ) == parseInt( k ) ) 
         {
          number_in_list++ ;
          ordered_list[ number_in_list ] = position_list[j][i] ;
         }
        if ( position_list[j][i] == k ) 
         {
          number_in_list++ ;
          ordered_list[ number_in_list ] = position_list[j][i] ;
         }
       }
     }
   }
 }
function getTheAttribute( theElement, theName )
 {
  /* ================================================================
  ** Author:  James D. Hawks
  ** Date:    2 December 2011
  ** Purpose: Return the current value of an attribute for the given element.
  */
  var i, theAttribute ;
  if ( theElement && ( (theElement.nodeType == 1 ) || 
                       (theElement.nodeType == 3 )  ) ) 
   {
    for ( i=0; i < theElement.attributes.length ; ++i )
     {
      if ( theElement.attributes[i].name.toLowerCase() == theName.toLowerCase() )
       {
        theAttribute = theElement.attributes[i].value ;
       }
     }
   }
  else
   {
    alert("ERROR: Either element is null, or nodeType is not 1 or3, in getTheAttribute") ;
   }
  return theAttribute ;
 }
function maximum_value ( numRow, numCol ) 
 {
  /* ================================================================
  ** Author:  James D. Hawks
  ** Date:    7 December 2020 
  ** Purpose: Determine value that can be entered, either as a known or unknown.
  ** Method:  Board size minus disabled positions.
  **          
  */
  var i, j, theMax=0, buttonIdString, theButtonElement ;
  theMax = numRow * numCol ;
  for ( j=0; j<numRow; ++j ) 
   {
    for ( i=0; i<numRow; ++i )
     {
      buttonIdString = "field_" + j.toString(10) + "_" + i.toString(10) ;
      theButtonElement  = window.document.getElementById( buttonIdString ) ;
      if ( 0 == parseInt( theButtonElement.getAttribute( "a_or_d" ) )  ) 
	   { 
        theMax-- ;
       }
     }
   }
  return theMax ;
 } 
function place_label_on_the_button( theButton, theString )
 {
  /* ================================================================
  ** Author:  James D. Hawks
  ** Date:    22, 23 May 2020
  ** Purpose: Place the string on the button as the label.
  **          Both textContent and innerHTML seem to work.
  **
  */
  if ( theButton )
   {	  
    if ( theButton.childNodes[0] ) 
	 {
      // 1st was recomended :
      //theButton.childNodes[0].nodeValue   = theString  ;
      theButton.childNodes[0].textContent   = theString  ;
      //theButton.childNodes[0].wholeText   = theString  ;
      //theButton.childNodes[0].text        = theString  ;	
      //theButton.childNodes[0].value       = theString  ;
      theButton.childNodes[0].innerHTML     = theString  ;
     }
    else if ( theButton.value ) 
	 {
      // 1st was recomended :
      //theButton.value       = theString  ;
      //theButton.nodeValue   = theString  ;
      theButton.textContent   = theString  ;
      //theButton.wholeText   = theString  ;
      //theButton.text        = theString  ;	
      theButton.innerHTML     = theString  ;
     }
    else 
	 {
      // 1st was recomended :
      theButton.innerHTML     = theString  ;
      //theButton.nodeValue   = theString  ;
      theButton.textContent   = theString  ;
      //theButton.wholeText   = theString  ;
      //theButton.text        = theString  ;	
      //theButton.value       = theString  ;
     }
   }
 }
function pre_solve( numRow, numCol, 
                    position_list, 
					minimum, maximum, 
					fixed_values_ordered, 
					contain_number_fixed_values, 
					visit_counting,
                    numbrix_or_hidato )
 {
  /* ================================================================
  ** Author:  James D. Hawks
  ** Date:    4/4/2020
  ** Purpose: - Copy the DOM data into an array.
  **          - Generate some other data as well.
  **          - Make sure ready to run, sanity checks.
  ** 
  */
  var i, j, k, index=0, valuePrevious, pass_or_fail = 1, pass_or_fail_two = 1 ;
  var buttonNameId, theButton, numberNonDisabled=0, maxValue=-1, minValue=99999 ;
  var theValues, numberFixed=0, badValue, error_string="" ; 
  /*
  ** Copy the game board information for each position to a separate array.
  ** Also initiate an array to count visits to a position.
  */
  for ( j=0; j<numRow; ++j )
   {
    position_list[j]  = {} ;
    visit_counting[j] = {} ;
    for ( i=0; i<numCol; ++i )
     {
      buttonNameId = "field_" + j.toString(10) + "_" + i.toString(10) ;
      theButton = window.document.getElementById( buttonNameId ) ;
      position_list[j][i] = new position ( j, i,           theButton.getAttribute( "a_or_d" ),
                                                           theButton.getAttribute( "fixed"  ),
                                                 parseInt( theButton.getAttribute( "fixed_value" ) ) ) ;
      visit_counting[j][i]    = {} ;
      visit_counting[j][i][0] = 0  ;
     }
   }		 
  /*
  ** Verify that number of non-disabled positions equals the highest number on board.
  ** Also return the row and column of the minimum and maximum fixed values,
  ** which are the starting and ending points for the search.
  */
  for ( j=0; j<numRow; ++j )
   {
    for ( i=0; i<numCol; ++i )
     {
      if ( parseInt( position_list[j][i].a_or_d ) == 1 ) numberNonDisabled++ ;
      if ( parseInt( position_list[j][i].fixed  ) == 1 ) numberFixed++       ;
      if ( maxValue < parseInt( position_list[j][i].value ) )
	   {
        // No need to check for disabled or fixed, because value set to zero there.
        maxValue = parseInt( position_list[j][i].value ) ;
        rowMax = j ;
        colMax = i ;
       }
      if ( parseInt( position_list[j][i].value ) < minValue && 
           parseInt( position_list[j][i].fixed ) == 1        )
	   {
        minValue = parseInt( position_list[j][i].value ) ;
        rowMin = j ;
        colMin = i ;
       }
     }
   }	   
  /*
  ** Numbrix may have the min and max not identified, but not sure yet.
  ** The Daily News newspaper provided Hidato's always give the location of the highest and lowest numbers,
  ** but some web sites documenting the game do not give min and max location.
  ** Allow missing min and max location for both puzzels by commenting this out this section.
  ** 
  if ( numberNonDisabled != maxValue && numbrix_or_hidato == 1 )
   {
    pass_or_fail = 0 ;
    error_string = "ERROR: Number non-disabled positions, " + numberNonDisabled.toString(10) +
      " not equal to highest number on game board, " + maxValue.toString(10) ;
   }
  if ( 1 != minValue && numbrix_or_hidato == 1 )
   {
    pass_or_fail  = 0 ;
    error_string += "ERROR: The minimum value, " + minValue.toString(10) + ", is not one." ;
   }
  */
  /*
  ** But we still need to ensure that highest number provided is not too big for board,
  ** which only allows a step size of one.
  */
  if ( numberNonDisabled < maxValue )
   {
    pass_or_fail = 0 ;
    // Convert tu user indices.
    j = rowMax + 1 ;
    i = colMax + 1 ;
    error_string = "ERROR: The number of non-disabled positions, " + numberNonDisabled.toString(10) +
      ", is less than the highest number on game board, " + maxValue.toString(10) + 
      ", at row " + j.toString(10) + " and column " + i.toString(10) + "." ;
   }
  contain_number_fixed_values[0] = numberFixed ;
  contain_number_fixed_values[1] = numberNonDisabled ;
  /*
  ** Save the min and max data in a manner that bypasses JavaScript being pass by value.
  */
  minimum[0] = rowMin ;
  minimum[1] = colMin ;
  minimum[2] = minValue ;
  maximum[0] = rowMax ;
  maximum[1] = colMax ;
  maximum[2] = maxValue ;
  /*
  ** Verify that no fixed value was used twice.
  */
  theValues = new Array( numberFixed ) ;
  for ( j=0; j<numRow; ++j )
   {
    for ( i=0; i<numCol; ++i )
     {
      if ( position_list[j][i].fixed == 1 )
	   {
        theValues[ index ] = parseInt( position_list[j][i].value ) ;
        index++ ;
       }
     }
   }
  theValues.sort() ;
  valuePrevious = -1 ;
  for ( i=0; i<theValues.length; ++i )
   {
    if ( theValues[i] == valuePrevious )  
	 {
      pass_or_fail_two = 0 ;
      badValue = theValues[i] ;
     }
    valuePrevious = theValues[i] ;
   }
  if ( pass_or_fail_two == 0 )
   {
    pass_or_fail =  0 ;
    error_string += "  ERROR: Fixed value entered twice, " + badValue.toString(10) ;
   }
  /*
  ** Get each number from minimum to maximum in ordered list.
  */
  if (  pass_or_fail == 1 ) 
   {   
    generateOrderedList( fixed_values_ordered, numRow, numCol, position_list, minValue, maxValue ) ;
   } 
  else window.document.the_form.communicate.value = error_string ;
  //
  return pass_or_fail ;
 }
function position ( theRow, theColumn, active_or_disabled, theFix, theValue )
 {
  /* ================================================================
  ** Author:  James D. Hawks
  ** Date:    4/4/2020
  ** Purpose: Store data for one position on game board in single structure.
  ** Method:  Notice that JavaScript does NOT use a return statement for this,
  **          Flanagan page 151.
  **
  */	 
  this.row      = theRow ;
  this.column   = theColumn ;
  this.a_or_d   = active_or_disabled ;
  this.fixed    = theFix ;
  this.value    = theValue ;
  // Works with and without this declaration :
  this.achieved = 0 ;
 } 
function remove_the_solution( numRow, numCol )
 {
  /* ================================================================
  ** Author:  James D. Hawks
  ** Date:    16 April 2020 
  ** Purpose: Hide the solved for values on the board, 
  **          thus returning the configuration to state before solution.
  **          Should allow picking up where left off when multiple solutions found.
  **
  **
  */
  var i, j, buttonNameId, theButton, a_or_d, fixed, fixed_value ;
  for ( j=0; j<numRow; ++j )
   {
    for ( i=0; i<numCol; ++i )
     {
      buttonNameId = "field_" + j.toString(10) + "_" + i.toString(10)     ;
      theButton    = window.document.getElementById( buttonNameId )       ;
      a_or_d       =           theButton.getAttribute( "a_or_d" )         ;
      fixed        =           theButton.getAttribute( "fixed"  )         ;
      fixed_value  = parseInt( theButton.getAttribute( "fixed_value" ) )  ;
      if ( a_or_d == 1 && fixed == 0 ) storeValue( j, i, 0 ) ;
     }
   }
 }
function search ( row,    column, 
                  numRow, numCol, 
                  position_list, 
                  minimum, maximum, 
                  fixed_values_ordered, 
                  contain_number_fixed_values, 
                  visit_counting,
                  numbrix_or_hidato,
				  search_up_or_down )
 {
  /* ================================================================
  ** Author:  James D. Hawks
  ** Date:    5 April 2020, but based on previous versions for different puzzles :
  **          10/10/10 for KenKen, 3 September 2005 for Sudoku.
  ** Translation to JavaScript: 21 January 2007
  ** Purpose: Well, SEARCH - brute force, try every possibility.
  ** Method:  Algorithm originally designed to find one solution and exit.
  **          Now handles multiple solutions.
  **          pass_or_fail was the flag used in original version to terminate
  **          the search when solution was found.  Now just reset to false after
  **          saving the solution so that search continues.
  **
  */
  var pass_or_fail = 0, row_new, column_new ;
  let stop_or_go, holder, valid, index_of_new_equals_fixed=-1 ;
  let i, j, k, c_low, c_high, r_low, r_high, index, temp, truncate_search = 0 ;
  let value_new, number_possible=-1 ;
  let rowMin, colMin, minValue, rowMax, colMax, maxValue ;
  let FO_value, FO_achieved, PL_value, numberFixed, numberNonDisabled ;
  let possibilities = {} ;
  //System.out.println("search top.");
  /*
  ** Two good diagnostic methods.
  ** The 2nd is better a generating a bunch of data quickly so you can see where the search is working.
  ** But the 1st really pins down a single case to enable examination of why a solution was bypassed.
  */
  // compare_board ( row, column, numRow, numCol, position_list, minimum, maximum  ) ;
  // count_visits  ( row, column, visit_counting, position_list[row][column].value ) ;
  /*
  ** Get these out of a pass by value avoider.
  */
  rowMin   = minimum[0] ;
  colMin   = minimum[1] ;
  minValue = minimum[2] ;
  rowMax   = maximum[0] ;
  colMax   = maximum[1] ;
  maxValue = maximum[2] ;
  /*
  ** It is possible to place a value that was equal to a fixed value 
  ** somewhere else, that had not yet been discovered by the search.
  ** On moderately sized cases this is O.K.  But long waits have been seen when large,
  ** and this could be a contributor.  Ensure it is not a factor.
  ** Force the search to find each fixed value before proceeding to the next.
  */
  // For some reason, this conditional does not execute correctly in debugger, so rewrite it.
  // That was the source of the slowness, extra values were not null.
  // j = 0 ;
  // while ( fixed_values_ordered[j] != null )
  //  {
  //   if ( ( parseInt( fixed_values_ordered[j].value )        < 
  //          parseInt( position_list[ row ][ column ].value )	) &&
  //        parseInt(   fixed_values_ordered[j].achieved != 1  )   )
  //    {
  //     truncate_search = 1 ;
  //    }
  // j++ ;
  //}
  numberFixed       = contain_number_fixed_values[0] ;
  numberNonDisabled = contain_number_fixed_values[1] ;
  for ( j=0; j<numberFixed; ++j )
   {
    FO_value    = parseInt( fixed_values_ordered[j].value ) ;
    FO_achieved = parseInt( fixed_values_ordered[j].achieved ) ;
    PL_value    = parseInt( position_list[ row ][ column ].value ) ;
    if ( FO_value < PL_value )
	 {
      if ( FO_achieved != 1 )
	   {
        truncate_search = 1 ;
       }
     }
   }
  /*
  ** Do the SEARCH.
  */
  if ( pass_or_fail == 0 && truncate_search == 0 ) 
   {
    /*
    ** Create a list of all possible next steps.
    ** Where the step is a move of a single square / position with an increase in value of one.
    ** So it must be an adjacent position, either horizontally, vertically or diagonally.
    ** Work left to right, top to bottom.
    */
    r_low  = row    - 1 ;
    c_low  = column - 1 ;
    if ( 0 == row    ) r_low = 0 ;
    if ( 0 == column ) c_low = 0 ;
    r_high = row    + 1 ;
    c_high = column + 1 ;
    if ( ( numRow - 1 ) == row    ) r_high = numRow - 1 ;
    if ( ( numCol - 1 ) == column ) c_high = numCol - 1 ;
    for ( j=r_low; j<=r_high; ++j )
	 {
      for ( i=c_low; i<=c_high; ++i )
	   {
        // This line removes selection of the current location as a valid move.
        if ( j != row || i != column )
		 {
          /*
          ** The game Numbrix differs from Hidato by the exclusion of diagonal moves.
          */
          if ( numbrix_or_hidato == 1 )
           {
            valid = canImoveThere( position_list[row][column].value, position_list[j][i], search_up_or_down ) ;
            if ( valid == 1 )
             {
              number_possible++ ;
			  possibilities[number_possible] = position_list[j][i] ;
             }
           }
          else
           {
            /*
            ** So we have to filter out the diagonal moves, right here, for Numbrix.
            */
            if ( j == row || i == column )
             {
              valid = canImoveThere( position_list[row][column].value, position_list[j][i], search_up_or_down ) ;
              if ( valid == 1 )
               {
                number_possible++ ;
			    possibilities[number_possible] = position_list[j][i] ;
               }
             }
           }
         }        
	   }
     }
    /*
    ** Loop over all the possibilities.
    */
    for ( k=0; k<=number_possible; ++k )
	 {
      if ( ( pass_or_fail == 0 )        && 
           ( possibilities[k] != null )  ) 
       {
        /*
        ** Since this value was set/assigned,
        ** move on to the next location.
        */
        row_new     = possibilities[k].row    ;
        column_new  = possibilities[k].column ;
        //value_new = position_list[row][column].value + 1 ;
        value_new   = position_list[row][column].value + search_up_or_down ;
        /*
        ** When the new position is not fixed, must place the value.
        */
        if ( position_list[row_new][column_new].fixed != 1 ) 
		 {
          position_list[row_new][column_new].value = value_new ;
         }
        else
		 {
          /*
          ** When it is fixed, record that it has been achieved.
          */
          for ( j=0; j<numberFixed; ++j )
		   {
            if ( fixed_values_ordered[j].value == value_new )
			 {
              fixed_values_ordered[j].achieved = 1 ;
              index_of_new_equals_fixed = j ;
             }
           }
		 }
        /*
        ** When we arrive at the destination.
        ** We might have the possibility of Numbrix cases that do not start and end at min and max.
        ** Was updated to allow Hidato to also not provide min and max locations, since some web sites have it that way.
        **
        if ( (   row_new == rowMax && column_new == colMax   && numbrix_or_hidato == 1 )                                    ||
             (   row_new == rowMax && column_new == colMax   && numbrix_or_hidato == 0 && numberNonDisabled == maxValue   ) ||
             (                                                  numbrix_or_hidato == 0 && numberNonDisabled == value_new  ) ||
			 ( ( row_new != rowMin || column_new != colMin ) && numbrix_or_hidato == 0 && value_new == 1 && 1 != minValue )  )
         {
        */
        if ( (   row_new == rowMax && column_new == colMax   && numberNonDisabled == maxValue   ) ||
             (                                                  numberNonDisabled == value_new  ) ||
			 ( ( row_new != rowMin || column_new != colMin ) && value_new == 1 && 1 != minValue )  )
         {
          /*
          ** When come to end, and Numbrix not start from value = 1, search from min down to 1, because grid not full.
		  ** Done at end so not consider positions occupied by the more easily controlled search between fixed values.
          ** The 4th conditional above terminated that downward search.
		  **
		  if ( ( ( row_new == rowMax && column_new == colMax   && numbrix_or_hidato == 0 && numberNonDisabled == maxValue   ) ||
                 (                                                numbrix_or_hidato == 0 && numberNonDisabled == value_new  )  ) &&
               1 != minValue                                                                                                      )
		   {
          */
		  if ( ( ( row_new == rowMax && column_new == colMax   && numberNonDisabled == maxValue   ) ||
                 (                                                numberNonDisabled == value_new  )  ) &&
               1 != minValue                                                                             )
		   {
            search_up_or_down = -1 ;
            pass_or_fail = search ( rowMin,  colMin, 
                                    numRow,  numCol, 
                                    position_list, 
                                    minimum, maximum, 
                                    fixed_values_ordered, 
                                    contain_number_fixed_values, 
                                    visit_counting,
                                    numbrix_or_hidato,
  								    search_up_or_down ) ;
            if (pass_or_fail == 0)
             {
              /*
              ** When back out of a configuration, reset the parameters placed upon entry, above.
              */
              search_up_or_down = 1 ;
              if ( position_list[row_new][column_new].fixed != 1 ) 
  		       {
                position_list[row_new][column_new].value = 0 ;
               }
              else
  		       {
                fixed_values_ordered[ index_of_new_equals_fixed ].achieved = 0 ;
               }
             }
		   }
          else
		   {
            /*
            ** Now evaluate whether this full grid is a solution.
            */
            pass_or_fail = solution ( numRow, numCol, position_list, rowMin, colMin, rowMax, colMax ) ;
            if (pass_or_fail == 1)
             {
              store_solution ( position_list, numRow, numCol ) ;
              /*
              ** Reset the solution flag so that the search may continue from this arrangement.
              ** In this case the next possibility will be tried.
              */
              pass_or_fail = 0 ; 
             }
            /*
            ** When back out of a configuration, reset the parameters placed upon entry, above.
            */
            if ( position_list[row_new][column_new].fixed != 1 ) 
             {
              position_list[row_new][column_new].value = 0 ;
             }
            else
             {
              fixed_values_ordered[ index_of_new_equals_fixed ].achieved = 0 ;
             }
		   }
         }
        else
         {
          pass_or_fail = search ( row_new, column_new, 
                                  numRow,  numCol, 
                                  position_list, 
                                  minimum, maximum, 
                                  fixed_values_ordered, 
                                  contain_number_fixed_values, 
                                  visit_counting,
                                  numbrix_or_hidato,
								  search_up_or_down ) ;
          if (pass_or_fail == 0)
           {
            /*
            ** When back out of a configuration, reset the parameters placed upon entry, above.
            */
            if ( position_list[row_new][column_new].fixed != 1 ) 
		     {
              position_list[row_new][column_new].value = 0 ;
             }
            else
		     {
              fixed_values_ordered[ index_of_new_equals_fixed ].achieved = 0 ;
             }
           }
         }
       }
     }
   }
  return pass_or_fail ;
 }
function setAD( event )
 {
  /* =========================================================
  ** Author :  James D. Hawks
  ** Date :    31 December 2010, 3 April 2020, 8 December 2020 
  ** Purpose : Store the active / disabled flags (a hidden value representing the last 
  **           selection from the main keypad) in selected location.
  **           This routine handles the left half of the board.
  **      
  **           This routine is called by the button press event.
  **           The actual call was in the HTML file, not this one.
  **
  */
  var theButtonThatWasClicked, findButtonFromName, buttonName, row, column, name_pieces ;
  var countButton,  textIdString = "t_field_" ;
  var numRow, numCol, numberUnknown, theTextValue, imageID ;
  /* 
  ** 
  */
  // Flanagan, page 396, failed, returned undefined nodeType/Name.
  //theButtonThatWasClicked = this ;
  // Instead try Murach page 487-489.
  if (!event) event = window.event ;
  if ( event.target )
   {
    // DOM
    theButtonThatWasClicked = event.target ;
   }
  else
   {
    // MS I.E.
    theButtonThatWasClicked = event.srcElement ;
   }
  //alert("in setAD, theButtonThatWasClicked =" + theButtonThatWasClicked ) ;
  //alert("theButtonThatWasClicked.nodeType nodeName =" +  theButtonThatWasClicked.nodeType + " - " + theButtonThatWasClicked.nodeName) ;
  // Next line failed, must not support overriding of methods with different number of arguments.
  // Renamed my routine to not conflict.
  //buttonName = theButtonThatWasClicked.getAttribute("name") ;
  // Next failed because button type was not "1," 
  // switched to getting the button from the event instead of the this keyword.
  //buttonName = getTheAttribute(theButtonThatWasClicked, "name") ;
  //buttonName = theButtonThatWasClicked.getAttribute("src") ;  this works in chrome where event is image object, not button
  if ( theButtonThatWasClicked.nodeName == "BUTTON" )
   {
    /*
    ** Both MS Internet Explorer and FireFox
    ** It looks like these web browsers returned the button in the event.
    */
    buttonName         = theButtonThatWasClicked.getAttribute("name") ;
    textIdString       = "t_" + buttonName ;
    findButtonFromName = theButtonThatWasClicked ;
   }
  else   if ( theButtonThatWasClicked.nodeName == "IMG" )
   {
    /*
    ** Chrome and maybe Safari
    ** It looks like these web browsers returned the image in the event.
    ** Not using image anymore, and Chrome had stopped this before that.
    */
    imageID = theButtonThatWasClicked.getAttribute("id") ;
    textIdString     = imageID ;
	/*
	** Get the button name, which did not have the "t_" prefix of the text name.
	*/
	buttonName = imageID.substring( 0, imageID.length ) ;
    findButtonFromName = window.document.getElementById( buttonName ) ;
   }
  else
   {
    alert("ERROR: Invalid theButtonThatWasClicked.nodeName in setAD.") ;
   }
  /*
  ** Save and display the activity / disabled information.
  */
  if ( 1 == parseInt( window.document.the_form.activate_disable.value ) ) 
   {
    /*
    ** Activate.
    */
    findButtonFromName.setAttribute("a_or_d", "1" ) ;
    findButtonFromName.setAttribute("class",       "button_left" ) ;
	theTextValue = parseInt( findButtonFromName.getAttribute( "fixed_value" ) ) ;
    if ( 0 == theTextValue ) 
	 {
      findButtonFromName.setAttribute( "fixed", 0 ) ;
      place_label_on_the_button( findButtonFromName, " " ) ;
     }
    //These don't work on their own, you need to change the class.
    //findButtonFromName.setAttribute("color",       "white" ) ;
    //findButtonFromName.setAttribute("background",  "white" ) ;
    /*
    ** Save the row column of this button.
    */
    name_pieces = buttonName.split( "_" ) ;
    window.document.the_form.current_row.value    = name_pieces[ 1 ] ;
    window.document.the_form.current_column.value = name_pieces[ 2 ] ;	
    /*
    ** Update the number of values to be determined, shown in button 13.
    */
    numRow        = parseInt( window.document.the_form.numberRows.value    ) ;
    numCol        = parseInt( window.document.the_form.numberColumns.value ) ;
    numberUnknown = count_unknown_values ( numRow, numCol ) ;
    countButton   = window.document.getElementById( "control_13" ) ;
    place_label_on_the_button( countButton, numberUnknown.toString(10) ) ;
    /*
    ** When this was an activation, set the prompt for entry of the value.
    ** Convert indices from internal to user values.
    */ 
	window.document.the_form.step_number.value = "4" ;
    window.document.the_form.communicate.value = 
        "Either use keypad below to enter the known number in that position, " + 
        "or leave it undefined so the computer will determine the value in the solution process.  ReStart will set value back to zero, which is undefined.  " +		
        "Hit 'Next Step' or another location when done with this position. " +
        "For row " + ( parseInt( name_pieces[1] ) + 1 )  + " column " + ( parseInt( name_pieces[2] ) + 1 ) + 
        " the current value is " + findButtonFromName.getAttribute( "fixed_value" ) ;				
   }
  else
   {
    /*
    ** Disable.
    */
    findButtonFromName.setAttribute("a_or_d",      "0"     ) ;
    findButtonFromName.setAttribute("fixed",       "0"     ) ;
    findButtonFromName.setAttribute("fixed_value", "0"     ) ;
    findButtonFromName.setAttribute("class",       "button_left_disabled" ) ;
    place_label_on_the_button( findButtonFromName, " " ) ;
    //These don't work on their own, you need to change the class.
    //findButtonFromName.setAttribute("color",       "black" ) ;
    //findButtonFromName.setAttribute("background",  "black" ) ;
    /*
    ** Update the number of values to be determined, shown in button 13.
    */
    numRow          = parseInt( window.document.the_form.numberRows.value    ) ;
    numCol          = parseInt( window.document.the_form.numberColumns.value ) ;
    numberUnknown   = count_unknown_values ( numRow, numCol ) ;
    countButton = window.document.getElementById( "control_13" ) ;
    place_label_on_the_button( countButton, numberUnknown.toString(10) ) ;
   }
 }
function setTheAttribute( theElement, theName, theValue )
 {
  /* ================================================================
  ** Author:  James D. Hawks
  ** Date:    2 December 2011
  ** Purpose: Set or replace the current value of an attribute for the given element.
  */
  var i, did_not_find_it = 1 ;
  if ( theElement && theElement.nodeType == 1 ) 
   {
    for ( i=0; i < theElement.attributes.length ; ++i )
     {
      if ( theElement.attributes[i].name.toLowerCase() == theName.toLowerCase() )
       {
        //alert("Inside setTheAttribute : theName, theValue = " + theName + " " + theValue) ;
        theElement.attributes[i].value = theValue ;
        did_not_find_it = -1 ;
       }
     }
    if ( did_not_find_it == 1 )
     {
      theElement.setAttribute( theName, theValue ) ;
     }
   }
  else
   {
    alert("ERROR: Either element is null, or nodeType is not 1, in setTheAttribute") ;
   }
 }
function setValue( theButton )
 {
  /* =========================================================
  ** Author :  James D. Hawks
  ** Date :    31 December 2010, 4 April 2020 
  ** Purpose : Store the value entered by user the in selected location.
  **           This routine handles the left half of the board.
  **      
  **           This is different than storeValue, because it does not take 
  **           row & column as arguments, but does take the button.
  **
  */
  var buttonName, theValue, a_or_d ;
  /*
  ** 
  */
  theValue  = parseInt( theButton.getAttribute( "fixed_value" ) );
  a_or_d    = parseInt( theButton.getAttribute( "a_or_d"      ) ) ;
  if ( a_or_d == 1 )
   {
    if ( 0 < theValue )  place_label_on_the_button( theButton, theValue.toString(10) ) ;
    else                 place_label_on_the_button( theButton, " " ) ;
   }
  else                   place_label_on_the_button( theButton, " " ) ;
 } 
function show_the_solution( numRow, numCol, which_solution )
 {
  /* ================================================================
  ** Author:  James D. Hawks
  ** Date:    5 April 2020 but based on 9 October 2011
  ** Purpose: Copy the board values at moment of solution into a single string.
  **          Save the string as the document variable, hidden input. 
  **
  **
  */
  var ts, i, j, temp, solution_string = "", index=0, fields_array ;
  switch ( which_solution ) 
   {
    case 1 :
      ts = window.document.getElementById("solution_01").value ;
      break ;
    case 2 :
      ts = window.document.getElementById("solution_02").value ;
      break ;
    case 3 :
      ts = window.document.getElementById("solution_03").value ;
      break ;
    case 4 :
      ts = window.document.getElementById("solution_04").value ;
      break ;
    case 5 :
      ts = window.document.getElementById("solution_05").value ;
      break ;
    case 6 :
      ts = window.document.getElementById("solution_06").value ;
      break ;
    case 7 :
      ts = window.document.getElementById("solution_07").value ;
      break ;
    case 8 :
      ts = window.document.getElementById("solution_08").value ;
      break ;
    case 9 :
      ts = window.document.getElementById("solution_09").value ;
      break ;
    case 10 :
      ts = window.document.getElementById("solution_10").value ;
      break ;
    case 11 :
      ts = window.document.getElementById("solution_11").value ;
      break ;
    case 12 :
      ts = window.document.getElementById("solution_12").value ;
      break ;
    case 13 :
      ts = window.document.getElementById("solution_13").value ;
      break ;
    case 14 :
      ts = window.document.getElementById("solution_14").value ;
      break ;
    case 15 :
      ts = window.document.getElementById("solution_15").value ;
      break ;
    case 16 :
      ts = window.document.getElementById("solution_16").value ;
      break ;
    case 17 :
      ts = window.document.getElementById("solution_17").value ;
      break ;
    case 18 :
      ts = window.document.getElementById("solution_18").value ;
      break ;
    case 19 :
      ts = window.document.getElementById("solution_19").value ;
      break ;
    case 20 :
      ts = window.document.getElementById("solution_20").value ;
      break ;
   }
  fields_array = ts.split ( "_" ) ;
  for (j=0; j<numRow; ++j)
   {
    for (i=0; i<numCol; ++i)
     {
      storeValue( j, i, parseInt( fields_array[ index ] ) ) ;
      index++ ;
     }
   }
 }
function solution ( numRow, numCol, position_list, rowMin, colMin, rowMax, colMax )
 {
  /* ================================================================
  ** Author:  James D. Hawks
  ** Date:    5 April 2020, derived from :
  **          10/10/10 mod for KenKen, but based on 3 September 2005 Sudoku.
  ** Purpose: Check if this arrangement is a solution.
  ** Note:    How can I check that this is a valid Hidato solution?
  **          I mean, isn't arriving at the destination the whole point,
  **          and you had to do that to get here?
  **          Guess I can check a few things.
  */
  var pass_or_fail = 1 ;
  let number_disabled = 0, number_active = 0 ;
  let i, j, k, minVal=99999, maxVal=-1, row, column, row_previous, column_previous ;
  //alert("subroutine solution : numCol=" + numCol + " numRow=" + numRow ) ;
  /*
  ** Verify that each disabled postion has no value, and each active position does.
  */
  for ( j=0; j < numRow; ++j )
   {
    for ( i=0; i < numCol; ++i )
     {
      if ( position_list[j][i].a_or_d == 0 && 
           position_list[j][i].value  != 0  ) number_disabled++ ;
      if ( position_list[j][i].a_or_d == 1 && 
           position_list[j][i].value  == 0  ) number_active++   ;
     }
   }
  if ( 0 < number_disabled || 0 < number_active ) pass_or_fail = 0 ;
  //alert("subroutine solution : after disabled no value, active with, pass_or_fail=" + pass_or_fail) ;
  if ( pass_or_fail == 1 )
   {
    /*
    ** Verify each number from minimum to maximum and adjacency.
    */
    minValue = position_list[ rowMin ][ colMin ].value ;
    maxValue = position_list[ rowMax ][ colMax ].value ;
    for ( k=minValue; k<=maxValue; ++k )
     {
      row = -1 ;
      for ( j=0; j < numRow; ++j )
       {
        for ( i=0; i < numCol; ++i )
         {
          if ( parseInt( position_list[j][i].value ) == k  ) 
		   {
            row    = j ;
            column = i ;
           }
         }
       }
      if ( row == -1 ) pass_or_fail = 0 ;
      else
       {
        if ( k != minValue )
		 {
          if ( 1 < Math.abs( row    - row_previous    ) ||
               1 < Math.abs( column - column_previous )  ) pass_or_fail = 0 ;
         }
        row_previous    = row    ;
        column_previous = column ;
       }
     }
    //alert("subroutine solution : after adjacency check, pass_or_fail=" + pass_or_fail) ;
   }
  return pass_or_fail ;
 }
function store_solution ( position_list, numRow, numCol ) 
 {
  /* =========================================================
  ** Author :  James D. Hawks
  ** Date :    5 April 2020 but based on 9 October 2011
  ** Purpose : Save the current board arrangement, the position within the search,
  **           as a document variable which persists.
  **           Do this because it was a solution, increment the solution counter.
  **           Because the value at a position may be greater than 9, 
  **           we have to implement a field separator in the solution string.
  **
  **
  */
  var i, j, ns, number_of_solutions, solution_string = "" ;
  //ns = window.document.getElementById("number_of_solutions") ;
  ns = document.getElementById("number_of_solutions") ;
  //ns = document.number_of_solutions ;
  /*
  ** A solution was located, increment the counter,
  ** Stored as persisting document variable and not passed number 
  ** because JavaScript is pass by reference.
  ** First set of value below works because is pointer to the document variable,
  ** however, also set it directly for the sake of browsers not tested.
  */
  number_of_solutions = ns.value ;
  number_of_solutions++ ;
  ns.value = number_of_solutions ;
  window.document.the_form.number_of_solutions.value = number_of_solutions ;
  /*
  ** Build a string of the values that comprise the solution.
  */
  for (j=0; j<numRow; ++j)
   {
    for (i=0; i<numCol; ++i)
     {
      solution_string = solution_string + position_list[j][i].value.toString(10) ;
      if ( j != (numRow -1) || i != (numCol -1) ) solution_string = solution_string + "_" ;
     }
   }
  /*
  ** Save the solution string as a document variable.
  */
  switch ( number_of_solutions ) 
   {
    case 1 :
      window.document.the_form.solution_01.value = solution_string ;
      break ;
    case 2 :
      window.document.the_form.solution_02.value = solution_string ;
      break ;
    case 3 :
      window.document.the_form.solution_03.value = solution_string ;
      break ;
    case 4 :
      window.document.the_form.solution_04.value = solution_string ;
      break ;
    case 5 :
      window.document.the_form.solution_05.value = solution_string ;
      break ;
    case 6 :
      window.document.the_form.solution_06.value = solution_string ;
      break ;
    case 7 :
      window.document.the_form.solution_07.value = solution_string ;
      break ;
    case 8 :
      window.document.the_form.solution_08.value = solution_string ;
      break ;
    case 9 :
      window.document.the_form.solution_09.value = solution_string ;
      break ;
    case 10 :
      window.document.the_form.solution_10.value = solution_string ;
      break ;
    case 11 :
      window.document.the_form.solution_11.value = solution_string ;
      break ;
    case 12 :
      window.document.the_form.solution_12.value = solution_string ;
      break ;
    case 13 :
      window.document.the_form.solution_13.value = solution_string ;
      break ;
    case 14 :
      window.document.the_form.solution_14.value = solution_string ;
      break ;
    case 15 :
      window.document.the_form.solution_15.value = solution_string ;
      break ;
    case 16 :
      window.document.the_form.solution_16.value = solution_string ;
      break ;
    case 17 :
      window.document.the_form.solution_17.value = solution_string ;
      break ;
    case 18 :
      window.document.the_form.solution_18.value = solution_string ;
      break ;
    case 19 :
      window.document.the_form.solution_19.value = solution_string ;
      break ;
    case 20 :
      window.document.the_form.solution_20.value = solution_string ;
      break ;
   }
  if  ( 20 < number_of_solutions ) window.document.the_form.solution_21.value = "More than Twenty solutions located." ;
 }
function storeValue( row, column, theValue )
 {
  /* =========================================================
  ** Author :  James D. Hawks
  ** Date :    31 December 2010
  ** Purpose : Store the value passed in at the location defined
  **           by the row and column.
  **           This value is for display purposes, 
  **           functional values either in button or position array.
  **
  **           This routine handles the left half of the board.
  **           Now the buttons contain a text value.
  **
  **           This routine was only called in this file.
  **           Was not the direct event processor called in the HTML file.
  **
  **           This is different than setValue, because it takes row & column
  **           as arguments, but not the button. 
  ** 
  **           Can NOT pass position_list down here to determine whether or not
  **           a button was disabled, because we are now one cycle later in the 
  **           user_interaction routine.  Position_list was declared, but not 
  **           populated this time through.  Have to get the information from  
  **           the buttons.
  ** 
  ** 
  */
  var theButtonElement, buttonIdString =   "field_",  a_or_d   ;
  buttonIdString  += row.toString(10) + "_" + column.toString(10) ;
  theButtonElement = window.document.getElementById( buttonIdString ) ;
  a_or_d           = parseInt( theButtonElement.getAttribute( "a_or_d" ) ) ;
  if ( a_or_d == 1 ) 
   {
    if ( 0 < theValue ) place_label_on_the_button( theButtonElement, theValue.toString(10) ) ;
    else                place_label_on_the_button( theButtonElement, " " ) ;
   }
  else                  place_label_on_the_button( theButtonElement, " " ) ;
 }
function user_interaction( button_pushed )
 {
  /* =========================================================
  ** Author :  James D. Hawks
  ** Date :    2-5 April 2020, based on similar routine in Sudoku Solver and KenKen.
  ** Purpose : This routine controls the progression through the different menus and options.
  **           Store or do something based on the last value selected from the keypad on right. 
  **           Notice that the text specified corresponds to the NEXT step number.
  ** 
  **           The routine is invoked as the "onclick" action for buttons on the right side,
  **           main menu of the board.  However, other routines can be invoked by buttons
  **           in the left half, game area, of the board.  Those buttons call the "setAD"
  **           routine and can, and do, modify the DOM variables used for control here.
  **           In particular, the step_number.value == 4 is activated only there.
  ** 
  ** 
  ** 
  */
  var numRow            = parseInt( window.document.the_form.numberRows.value        ) ;
  var numCol            = parseInt( window.document.the_form.numberColumns.value     ) ;
  var numbrix_or_hidato = parseInt( window.document.the_form.numbrix_or_hidato.value ) ;
  var position_list     = {}, fixed_values_ordered = {}, visit_counting = {} ;
  var minimum           = new Array(3), maximum = new Array(3), contain_number_fixed_values = {} ;
  let i, j, cs, ns, pass_or_fail, number_of_solutions, countButton, search_up_or_down = 1 ;
  let theRow, theColumn, button_name, theButton, numberUnknown, which_game_name ;
  let maxmimum_dimension = 20, temp_max = -1, temp_val = 0 ;
  //System.out.println("\n routine userInteraction : TOP \n") ;
  //alert("\n routine userInteraction : TOP \n") ;
  // =================================================
  if (window.document.the_form.step_number.value == 1)
   {
    /* 
    ** If value greater than 1000, just reset it to zero.  Someone is trying to break it.
    */
	if ( 1000 < parseInt( window.document.the_form.numberColumns.value ) ) window.document.the_form.numberColumns.value = 0 ;
    /* 
    ** Just beginning, store the number of columns, prompt for number of rows.
    */
    if ((-1 < button_pushed) && (button_pushed < 10))
     {	 
      window.document.the_form.numberColumns.value = (window.document.the_form.numberColumns.value * 10) + button_pushed ;
      window.document.the_form.communicate.value   = 
        "Use keypad below to enter the number of vertical columns on the board.  Current value is " + 
		window.document.the_form.numberColumns.value + ".  Hit 'Next Step' when done.  The number of columns and rows taken together should describe a rectangle enclosing each position." ;
     }
    else if ( button_pushed == 10 ) window.document.the_form.activate_disable.value = 0 ;
    else if ( button_pushed == 11 ) window.document.the_form.activate_disable.value = 1 ;
    else if ( ( 0 == parseInt( window.document.the_form.numberColumns.value) ) && 
	          ( ( button_pushed == 12 ) || ( button_pushed == 14 ) )            )
     {
      window.document.the_form.communicate.value = 
        "Chose a number from one to nine as the first digit.  Define the number of vertical columns on the board by selecting the integer from keypad below.  " +
		"The number of columns and rows taken together should describe a rectangle enclosing each position." ;
     }
	else if ( 0 != parseInt( window.document.the_form.numberColumns.value)  && 12 == button_pushed )
     {
      window.document.the_form.numberColumns.value = 0 ;
	  window.document.the_form.communicate.value   = 
        "Use keypad below to enter the number of vertical columns on the board.  Current value is " + 
		window.document.the_form.numberColumns.value + ".  Hit 'Next Step' when done.  The number of columns and rows taken together should describe a rectangle enclosing each position." ;
     }
	else if ( 0 != parseInt( window.document.the_form.numberColumns.value ) && 14 == button_pushed )  
     {
      if ( parseInt( window.document.the_form.numberColumns.value ) <= maxmimum_dimension )
	   {
        window.document.the_form.communicate.value = 
          "Now enter the number of horizontal rows on the board using the keypad.  Current value is " + 
          window.document.the_form.numberRows.value + ".  Hit 'Next Step' when done.  The number of columns and rows taken together should describe a rectangle enclosing each position." ;     
        window.document.the_form.step_number.value   = "2" ;
       }
      else
	   {
        window.document.the_form.numberColumns.value = 0 ;
        window.document.the_form.communicate.value = "Value too large.  Must be less than or equal to " + maxmimum_dimension.toString(10) +
          ". Use keypad below to enter the number of vertical columns on the board.  Current value is " + 
          window.document.the_form.numberColumns.value + ".  Hit 'Next Step' when done.  The number of columns and rows taken together should describe a rectangle enclosing each position." ;
	   }
	 }
   }
  // ======================================================
  else if (window.document.the_form.step_number.value == 2)
   {
    /* 
    ** If value greater than 1000, just reset it to zero.  Someone is trying to break it.
    */
	if ( 1000 < parseInt( window.document.the_form.numberRows.value ) ) window.document.the_form.numberRows.value = 0 ;
    /* 
    ** Store the number of rows, move on to element disable and activate.
    */
    if ((-1 < button_pushed) && (button_pushed < 10))
     {
	  window.document.the_form.numberRows.value = (window.document.the_form.numberRows.value * 10) + button_pushed ;
      window.document.the_form.communicate.value = 
        "Now enter the number of horizontal rows on the board using the keypad.  Current value is " + 
		window.document.the_form.numberRows.value + ".  Hit 'Next Step' when done.  The number of columns and rows taken together should describe a rectangle enclosing each position." ;
     }
    else if ( button_pushed == 10 ) window.document.the_form.activate_disable.value = 0 ;
    else if ( button_pushed == 11 ) window.document.the_form.activate_disable.value = 1 ;
    else if ( ( 0 == parseInt( window.document.the_form.numberRows.value) ) && 
	          ( ( button_pushed == 12 ) || ( button_pushed == 14 ) )         )
     {
      window.document.the_form.communicate.value = 
        "Chose a number from one to nine as the first digit.  Define the number of horizontal rows on the board by selecting the integer from keypad below." +
		"  The number of columns and rows taken together should describe a rectangle enclosing each position." ;
     }
	else if ( 0 != parseInt( window.document.the_form.numberRows.value)  && 12 == button_pushed )
     {
      window.document.the_form.numberRows.value  = 0 ;
	  window.document.the_form.communicate.value = 
        "Use keypad below to enter the number of horizontal rows on the board.  Current value is " + 
		window.document.the_form.numberRows.value + ".  Hit 'Next Step' when done.  The number of columns and rows taken together should describe a rectangle enclosing each position." ;
     }
	else if ( 0 != parseInt( window.document.the_form.numberRows.value ) && 14 == button_pushed )  
     {
      if ( parseInt( window.document.the_form.numberRows.value ) <= maxmimum_dimension )
	   {
        /*
        ** Generate a table of buttons, to left of keypad, of the specified number rows and columns.
        */
        create_the_board() ; 
        // Prompt for next step.
        window.document.the_form.communicate.value = 
          "Use the Activate and Disable buttons on the keypad to control which positions are in play, on the new board to the left.  " +  
          "Hit the button, then touch the board everywhere it applies.  When activating a button, you will be prompted for a value in that position.  " +
          "When complete and board looks like game, hit Next Step.  "  +
		  "Start whole game over again with ReStart button."  ;
        window.document.the_form.step_number.value = "3" ;
	   }
      else
	   {
        window.document.the_form.numberRows.value = 0 ;
        window.document.the_form.communicate.value = "Value too large.  Must be less than or equal to " + maxmimum_dimension.toString(10) + 
          ". Use keypad below to enter the number of horizontal rows on the board.  Current value is " + 
          window.document.the_form.numberRows.value + ".  Hit 'Next Step' when done.  The number of columns and rows taken together should describe a rectangle enclosing each position." ;
	   }
	 }
   }
  // ======================================================
  else if (window.document.the_form.step_number.value == 3)
   {
    /* 
    ** Let the user identify elements as active or disabled.
    ** Digits 0-9 not relevant here.
    */
    if      ( button_pushed == 10 ) window.document.the_form.activate_disable.value = 0 ;
    else if ( button_pushed == 11 ) window.document.the_form.activate_disable.value = 1 ;
    else if ( button_pushed == 12 )
     {
      clear ( ) ;
     }
    else if ( button_pushed == 14 ) 
     {
      /*
      ** User should have continued disabling and placing values until game board 
      ** achieved same arrangement as published configuration.
      ** So when done here, start the solution process.
      */
      which_game_name = "Numbrix." ;
      if ( parseInt( window.document.the_form.numbrix_or_hidato.value ) == 1 ) which_game_name = "Hidato." ;
      window.document.the_form.step_number.value = 5 ;
      window.document.the_form.communicate.value = 
        "Hidato allows diagonal moves, Numbrix does not.  Do you want to allow diagonal moves?  " +
        "Select number \"1\" on the keypad for Numbrix, no diagonals, or \"3\" for Hidato.  " +
        "The current value is " + which_game_name +
        " Hit \"ReStart\" to go back to entering numbers, or \"Next Step\" to continue Solution process.  " ;
     }
   }
  // ======================================================
  else if (window.document.the_form.step_number.value == 4)
   {
    /*
    ** You get to here from the "setAD" routine,
    ** which was activated by hitting button 11 above, step 3, 
    ** and then a button on the playing board.	
    ** 
    ** User has Activated a position on playing board, left half.
    ** He can either enter a known value, in which case that value is fixed
    ** during the soluton process, or leave blank / 0, so that the
    ** value will be determined during solution.
    ** Prompt the user, receive and store value, even if zero.
    ** Convert row, column indices from internal to user values.
    */
    theRow      = window.document.the_form.current_row.value    ;
    theColumn   = window.document.the_form.current_column.value ;
    button_name = "field_" + theRow + "_" + theColumn           ;
    theButton   = window.document.getElementById( button_name ) ;
    temp_max    = maximum_value ( numRow, numCol )              ; 
    if ((-1 < button_pushed ) && (button_pushed < 10))
     {
	  temp_val = ( theButton.getAttribute( "fixed_value" ) * 10 ) + button_pushed ;
      if ( temp_val <= temp_max ) 
	   {
        theButton.setAttribute( "fixed", "1" ) ;	 
	    theButton.setAttribute( "fixed_value", temp_val ) ;
        window.document.the_form.communicate.value = 
          "Either use keypad below to enter the known number in that position, " + 
          "or leave it undefined so the computer will determine the value in the solution process.  ReStart will set value back to zero, which is undefined.  " +
          " Hit 'Next Step' or another location when done with this position. " +
          "For row " + ( parseInt(theRow) + 1 )  + " column " + ( parseInt( theColumn ) + 1 ) + 
          " the current value is " + theButton.getAttribute( "fixed_value" ) ;
        // Put the number on the button.
        setValue( theButton )

       }
      else
	   {
		window.document.the_form.communicate.value = "ERROR: Value " + temp_val.toString(10) + " too big!!  " + 
		  "Please enter a number less than, or equal to " + temp_max.toString(10) + 
		  ", or hit Restart." ;		   
        // Rather than leave them with one digit less than they expected, zero out the entry.
        theButton.setAttribute( "fixed", "0" ) ;	 
	    theButton.setAttribute( "fixed_value", 0 ) ;
		setValue( theButton )
	   }
     }	     
    else if ( button_pushed == 10 ) window.document.the_form.activate_disable.value = 0 ;
    else if ( button_pushed == 11 ) window.document.the_form.activate_disable.value = 1 ;
    else if ( button_pushed == 12 ) 
	 {
      theButton.setAttribute( "fixed", 0 ) ;
      theButton.setAttribute( "fixed_value",   0 ) ;
      window.document.the_form.communicate.value = 
        "Either use keypad below to enter the known number in that position, " + 
        "or leave it undefined so the computer will determine the value in the solution process.  ReStart will set value back to zero, which is undefined.  " +
        " Hit 'Next Step' or another location when done with this position. " +
        "For row " + ( parseInt(theRow) + 1 )  + " column " + ( parseInt( theColumn ) + 1 ) + 
        " the current value is " + theButton.getAttribute( "fixed_value" ) ;
        // When user zeros out the value, you have to update the display.
        setValue( theButton )
     }
    else if ( button_pushed == 14 ) 
     {
      /*
      ** When done go back to step 3 to cover rest of game board.
      */
      window.document.the_form.communicate.value = 
		"Use the Activate and Disable buttons on the keypad to control which positions are in play, on the new board to the left.  " +  
        "Hit the button, then touch the board everywhere it applies.  When complete and board looks like game, hit Next Step.  "  +
		"Start whole game over again with ReStart button."  ;
	  window.document.the_form.step_number.value   = "3" ;
     }
    /*
    ** Regardless of what selection was made,	
    ** Update the number of values to be determined, shown in button 13.
    */
    numberUnknown = count_unknown_values ( numRow, numCol ) ;
    countButton   = window.document.getElementById( "control_13" ) ;
    place_label_on_the_button( countButton, numberUnknown.toString(10) ) ;
   }
  // ======================================================
  else if (window.document.the_form.step_number.value == 5)
   {
    /*
    ** Now that we are allowing Numbrix, 
    ** we have to prompt for whether it is Numbrix or Hidato.
    */ 
    if ( 1 == button_pushed) 
     {
      window.document.the_form.numbrix_or_hidato.value = 0 ;
      which_game_name = "Numbrix." ;
      window.document.the_form.step_number.value = 5 ;
      window.document.the_form.communicate.value = 
        "Hidato allows diagonal moves, Numbrix does not.  Do you want to allow diagonal moves?  " +
        "Select number \"1\" on the keypad for Numbrix, no diagonals, or \"3\" for Hidato.  " +
        "The current value is " + which_game_name +
        " Hit \"ReStart\" to go back to entering numbers, or \"Next Step\" to continue Solution process.  " ;
     }
    if ( 3 == button_pushed) 
     {
      window.document.the_form.numbrix_or_hidato.value = 1 ;
      which_game_name = "Hidato." ;
      window.document.the_form.step_number.value = 5 ;
      window.document.the_form.communicate.value = 
        "Hidato allows diagonal moves, Numbrix does not.  Do you want to allow diagonal moves?  " +
        "Select number \"1\" on the keypad for Numbrix, no diagonals, or \"3\" for Hidato.  " +
        "The current value is " + which_game_name +
        " Hit \"ReStart\" to go back to entering numbers, or \"Next Step\" to continue Solution process.  " ;
     }
    else if ( 12 == button_pushed ) 
     {
      /*
      ** When entering the numbers, 
      ** it is common to hit next step before ready to continue.
      ** Have to have a way, right here, to return to previous menu.
      ** So override the restart button.
      */
      window.document.the_form.step_number.value = "3" ;
      window.document.the_form.communicate.value = 
		"Use the Activate and Disable buttons on the keypad to control which positions are in play, on the new board to the left.  " +  
        "Hit the button, then touch the board everywhere it applies.  When activating a button, you will be prompted for a value in that position.  " +
        "When complete and board looks like game, hit Next Step.  "  +
		"Start whole game over again with ReStart button."  ;
     }
    else if ( 14 == button_pushed ) 
     {
      /*
      ** User should have continued disabling and placing values until game board 
      ** achieved same arrangement as published configuration.
      ** So when done here, start the solution process.
      */
      window.document.the_form.step_number.value = 6 ;
      window.document.the_form.communicate.value = 
        "Starting the solution process.  Hit 'NextStep' again to continue, this could take a few moments. "  +
        " Hit 'ReStart' to go back to entering numbers.  " ;
     }
   }
  // ======================================================
  else if (window.document.the_form.step_number.value == 6)
   {
    if      ( button_pushed == 12 )
     {
      /*
      ** When entering the numbers, 
      ** it is common to hit next step before ready to continue.
      ** Have to have a way, right here, to return to previous menu.
      ** So override the restart button.
      */
      window.document.the_form.step_number.value   = "3" ;
      window.document.the_form.communicate.value = 
		"Use the Activate and Disable buttons on the keypad to control which positions are in play, on the new board to the left.  " +  
        "Hit the button, then touch the board everywhere it applies.  When activating a button, you will be prompted for a value in that position.  " +
        "When complete and board looks like game, hit Next Step.  "  +
		"Start whole game over again with ReStart button."  ;
     }
    else if ( button_pushed == 14 )
     {
      /* 
      ** Compute and present the result.
      */
      window.document.the_form.communicate.value = "Solution in progess . . . " ;
      pass_or_fail = pre_solve( numRow, numCol, 
                                position_list, 
							    minimum, maximum, 
								fixed_values_ordered,
								contain_number_fixed_values, 
								visit_counting,
                                numbrix_or_hidato ) ;
      if (pass_or_fail != 1)
       {
        /*
        ** When pre_solve failed go to step 9.
        */
        window.document.the_form.communicate.value += "  Hit Next Step."  ;
	    window.document.the_form.step_number.value  = "9" ;
       }
      else 
       {
        /*
        ** Call the soluton methodology.
        */
        fixed_values_ordered[0].achieved = 1 ;
        pass_or_fail = search ( rowMin, colMin, 
                                numRow, numCol, 
                                position_list, 
                                minimum, maximum, 
                                fixed_values_ordered, 
                                contain_number_fixed_values, 
                                visit_counting,
                                numbrix_or_hidato,
								search_up_or_down ) ;
        ns = window.document.getElementById("number_of_solutions") ;
        number_of_solutions = ns.value ;
        if ( number_of_solutions < 1 )
         {
          window.document.the_form.communicate.value = "FAILURE: Solution not found!  Hit Next Step." ;
          window.document.the_form.step_number.value = 9 ;
         }
        else if ( number_of_solutions == 1 )
         {
          show_the_solution( numRow, numCol, 1 ) ;
          window.document.the_form.communicate.value = "Here is the result.  Select Restart or Next Step to run it again." ;
          /*
          **  Need to reset the number of solutions because another hit of the run button 
          **  will find the same solution, again, set the number of solutions to 2 and raise the error 
          **  on the other branch of this conditional.
          */
          window.document.the_form.number_of_solutions.value = "0";
          window.document.the_form.step_number.value = 7 ;
         }
        else if ( 1 < number_of_solutions )
         {
          window.document.the_form.communicate.value = "IMPORTANT: The number of solutions found was " + number_of_solutions + "!" +
            "\n\nYou should probably write a letter to the publisher of the puzzle you are solving.  " +
            "  Hit the Next Step button again to see the first solution." ;
          window.document.the_form.current_solution.value = 1 ;
          window.document.the_form.step_number.value = 8 ;
         }  
       }  
     }
   }
  // ======================================================
  else if (window.document.the_form.step_number.value == 7)
   {
    if ((-1 < button_pushed) && (button_pushed < 12))
     {
      window.document.the_form.step_number.value = 7 ;
     }
    else if ( ( 12 == button_pushed ) || ( 14 == button_pushed ) )
     {
      clear ( ) ;
     }
   }
  // ======================================================
  else if (window.document.the_form.step_number.value == 8)
   {
    /*
    ** Loop through stored solutions when multiple found.
    ** Buttons on left side of board not relevant.
    */
    if (12 == button_pushed)
     {
      clear ( ) ;
     }
    if (14 == button_pushed)
     {
      cs = parseInt(window.document.the_form.current_solution.value) ;
      ns = parseInt(window.document.the_form.number_of_solutions.value) ;
      if ((-1 < cs) && (cs < 21))
       {
        if      (cs < ns)
         {
          window.document.the_form.communicate.value = "Here is result number " + cs + ".  Hit the Next Step button to see the next solution." ;
          show_the_solution( numRow, numCol, cs ) ;
          window.document.the_form.current_solution.value = cs + 1 ;
         }
        else if (cs == ns)
         {
          window.document.the_form.communicate.value = "Here is result number " + cs + ".  It is the last solution.  Hit the Next Step again to return to configuration before starting solution." ;
          show_the_solution( numRow, numCol, cs ) ;
          window.document.the_form.current_solution.value = cs + 1 ;
         }
        else if (cs > ns)
         {
          //clear ( ) ;
          remove_the_solution( numRow, numCol ) ;
          window.document.the_form.step_number.value = "3" ;
          window.document.the_form.communicate.value = 
		    "Use the Activate and Disable buttons on the keypad to control which positions are in play, on the board to the left.  " +  
            "Hit the button, then touch the board everywhere it applies.  When activating a button, you will be prompted for a value in that position.  " +
            "When complete and board looks like game, hit Next Step.  "  +
		    "Start whole game over again with ReStart button."  ;
          clear_solutions( ) ;
         }
       }
      else
       {
        remove_the_solution( numRow, numCol ) ;
        window.document.the_form.communicate.value = "Twenty solutions was plenty.  It was all that this program was designed to display.  " +
          "Complain!  Write a letter. You have been returned to the configuration before you started the solution process.  " + 
		  "Use the Activate and Disable buttons on the keypad to control which positions are in play, on the board to the left.  " +  
          "Hit the button, then touch the board everywhere it applies.  When activating a button, you will be prompted for a value in that position.  " +
          "When complete and board looks like game, hit Next Step.  "  +
		  "Start whole game over again with ReStart button."  ;
        window.document.the_form.step_number.value = "3" ;
        clear_solutions( ) ;
       }
     }
   }
  // ======================================================
  else if (window.document.the_form.step_number.value == 9)
   {
    /*
    ** When solution or pre_solve failed, and we displayed failure message to user,
    ** we need an in between page to place the correct prompt for eventual landing page,
    ** so while we are here, tell him that he is headed back to defining the board.
    */
    window.document.the_form.communicate.value = 
      "You are being returned to game board as it was when you started the solution process.  " +
      "You will get to correct the configuration before trying again.  Hit the ReStart button."  ;	   
    if ( ((-1 < button_pushed) && (button_pushed < 12)) || ( 14 == button_pushed ) )
     {
      window.document.the_form.step_number.value = 9 ;
     }
    else if ( 12 == button_pushed )
     {
      window.document.the_form.step_number.value = "3" ;
      window.document.the_form.communicate.value = 
		"Use the Activate and Disable buttons on the keypad to control which positions are in play, on the board to the left.  " +  
        "Hit the button, then touch the board everywhere it applies.  When activating a button, you will be prompted for a value in that position.  " +
        "When complete and board looks like game, hit Next Step.  "  +
		"Start whole game over again with ReStart button."  ;
     }
   }   
 } 